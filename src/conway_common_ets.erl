-module(conway_common_ets).
-include("conway.hrl").

-export([build_board/2,
         cell_state/2,
         do/2]).

-define(ETS_OPTS,
        [ordered_set, public, {keypos, 1},
         {write_concurrency, true}, {read_concurrency, true}]).

%% Public
-spec build_board(pos_integer(), list(cell_pos())) -> board().
build_board(N, LiveCells) ->
    Tid = ets:new(conway, ?ETS_OPTS),
    ets:insert(Tid, [{Cell, on} || Cell <- LiveCells]),
    #board{size = N, board = Tid}.

-spec cell_state(cell_pos(), board()) -> cell_state().
cell_state(Cell, #board{board = Board}) ->
    case ets:lookup(Board, Cell) of
        [] -> off;
        _ -> on
    end.

-spec do(cell_pos(), board()) -> cell_state().
do(Cell, State = #board{size = N}) ->
    CellState = cell_state(Cell, State),
    Neighbors = conway_common:enumerate_neighbors(Cell, N),
    NeighborStates = [cell_state(C, State) || C <- Neighbors],
    LiveNeighbors = conway_common:count_neighbors(NeighborStates),
    conway_common:next_state(CellState, LiveNeighbors).
