-module(conway_seq).
-include("conway.hrl").

-export([get_generation/2,
         next_generation/1]).

%% Public

-spec get_generation(non_neg_integer(), board()) -> board().
get_generation(0, Board) -> Board;
get_generation(G, Board) ->
    NewBoard = next_generation(Board),
    get_generation(G - 1, NewBoard).

%% Private

-spec next_generation(board()) -> board().
next_generation(State = #board{size = N, board = B}) when is_tuple(B) ->
    LiveCells = [{Y, X} || X <- lists:seq(1, N),
                           Y <- lists:seq(1, N),
                           conway_common_tt:do({Y, X}, State) =:= on],
    conway_common_tt:build_board(N, lists:sort(LiveCells));
next_generation(State = #board{size = N, board = Tid}) ->
    LiveCells = [{Y, X} || {{Y, X}, _} <- ets:tab2list(Tid),
                           conway_common_ets:do({Y, X}, State) =:= on],
    conway_common_ets:build_board(N, lists:sort(LiveCells)).
