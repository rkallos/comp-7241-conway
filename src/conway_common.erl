-module(conway_common).
-include("conway.hrl").

-export([count_neighbors/1,
         enumerate_neighbors/2,
         glider/2,
         next_state/2,
         print_board/1,
         random_board/2]).

%% Public
-spec count_neighbors(list(cell_state())) -> non_neg_integer().
count_neighbors(List) -> count_neighbors(List, 0).

-spec enumerate_neighbors(cell_pos(), pos_integer()) -> [cell_pos()].
enumerate_neighbors({Y, X}, N) ->
    [{wrap(Y + A, N), wrap(X + B, N)} || A <- [-1, 0, 1], B <- [-1, 0, 1]]
        -- [{Y, X}].

-spec glider(non_neg_integer(), atom()) -> board() | {error, badarg}.
glider(N, _Module) when N < 3 ->
    {error, badarg};
glider(N, Module) ->
    LiveCells = [{1,2}, {2,3}, {3,1}, {3,2}, {3,3}],
    Module:build_board(N, LiveCells).

-spec next_state(cell_state(), non_neg_integer()) -> cell_state().
next_state(off, 3) -> on;
next_state(off, _) -> off;

next_state(on, 2) -> on;
next_state(on, 3) -> on;
next_state(on, _) -> off.

-spec print_board(board()) -> string().
print_board(Board = #board{board = B}) when is_tuple(B) ->
    print_board(Board, conway_common_tt);
print_board(Board) ->
    print_board(Board, conway_common_ets).

-spec random_board(pos_integer(), number()) -> list(cell_pos()).
random_board(N, P) ->
    Fun = fun({Y, X}, Acc) ->
                  Rand = rand:uniform(),
                  case Rand =< P of
                      true -> [{Y, X} | Acc];
                      false -> Acc
                  end
          end,
    lists:foldl(Fun, [], [{Y, X} || Y <- lists:seq(N,1,-1),
                                    X <- lists:seq(N,1,-1)]).

%% Private
count_neighbors([], Result) -> Result;
count_neighbors([on | Rest], Result) ->
    count_neighbors(Rest, Result + 1);
count_neighbors([_ | Rest], Result) ->
    count_neighbors(Rest, Result).

print_board(Board = #board{size = N}, Module) ->
    VertBorder = "+" ++ string:chars($-, N, "+\n"),
    Rows = ["|" ++
                [print_cell(Module:cell_state({Y, X}, Board))
                 || X <- lists:seq(1,N)]
            ++ "|\n"
            || Y <- lists:seq(1,N)],
    io:format("~s~s~s", [VertBorder, Rows, VertBorder]).

print_cell(off) -> $ ;
print_cell(on) -> $*.

wrap(M, N) when M =:= N + 1 -> 1;
wrap(0, N) -> N;
wrap(X, N) -> X rem (N + 1).

