-module(conway_erlangy).

-include("conway.hrl").

-record(state, {board = undefined :: board(),
                gen   = 0         :: pos_integer(),
                map   = #{}       :: #{cell_pos() => pid()}}).

-export([get_generation/2]).

%% Public

-spec get_generation(non_neg_integer(), board()) -> board().
get_generation(G, Board) ->
    #board{size = N, board = B} = Board,
    Module = case is_tuple(B) of
                 true  -> conway_common_tt;
                 false -> conway_common_ets
             end,
    Pids = [{{Y, X}, spawn_link(fun () -> conway_worker:init() end)}
            || Y <- lists:seq(1, N),
               X <- lists:seq(1, N)],
    PidMap = maps:from_list(Pids),
    State = #state{board = Board,
                   gen = G,
                   map = PidMap},
    [start_worker(Pid, Pos, Module, State) || {Pos, Pid} <- Pids],
    FinalStates =
        [receive
             {Pid, G, CellState} -> {Pos, CellState}
         end || {Pos, Pid} <- lists:sort(maps:to_list(PidMap))],
    LiveCells = [Pos || {Pos, CellState} <- FinalStates, CellState =:= on],
    Module:build_board(N, LiveCells).

%% Private

start_worker(Pid, Pos, Module, State) ->
    #state{gen = Gen, board = Board, map = PidMap} = State,
    #board{size = N} = Board,
    Neighbors = get_neighbor_pids(Pos, PidMap, N),
    CellState = Module:cell_state(Pos, Board),
    Options = #{neighbors  => Neighbors,
                pos        => Pos,
                state      => CellState,
                target_gen => Gen},
    Pid ! {self(), Options}.

get_neighbor_pids({Y, X}, PidMap, N) ->
    Neighbors = conway_common:enumerate_neighbors({Y, X}, N),
    [maps:get(Neighbor, PidMap) || Neighbor <- Neighbors].
