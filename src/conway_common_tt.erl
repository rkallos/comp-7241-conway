-module(conway_common_tt).
-include("conway.hrl").

-export([build_board/2,
         cell_state/2,
         do/2]).

%% Public
-spec build_board(pos_integer(), list(cell_pos())) -> board().
build_board(N, LiveCells) ->
    List = build_board({1,1}, N, LiveCells),
    Rows = build_rows(N, List),
    Board = list_to_tuple([list_to_tuple(Row) || Row <- Rows]),
    #board{size = N,
           board = Board}.

-spec cell_state(board(), cell_pos()) -> cell_state().
cell_state({Y, X}, #board{board = Board}) ->
    Row = erlang:element(Y, Board),
    erlang:element(X, Row).

-spec do(cell_pos(), board()) -> cell_state().
do(Cell, State = #board{size = N}) ->
    CellState = cell_state(Cell, State),
    Neighbors = conway_common:enumerate_neighbors(Cell, N),
    NeighborStates = [cell_state(C, State) || C <- Neighbors],
    LiveNeighbors = conway_common:count_neighbors(NeighborStates),
    conway_common:next_state(CellState, LiveNeighbors).

%% Private

% End of board
build_board({M, _}, N, _) when M > N -> [];
% End of row
build_board({Y, M}, N, Cells) when M > N ->
    build_board({Y + 1, 1}, N, Cells);
% No more live cells
build_board({Y, X}, N, []) ->
    [off | build_board({Y, X + 1}, N, [])];
% Live cell
build_board({Y, X}, N, [{Y, X} | Cells]) ->
    [on | build_board({Y, X + 1}, N, Cells)];
% Dead cell
build_board(C1 = {Y, X}, N, Cells = [C2 | _]) when C2 > C1 ->
    [off | build_board({Y, X + 1}, N, Cells)].

build_rows(_, []) -> [];
build_rows(N, List) ->
    {Row, Tail} = lists:split(N, List),
    [Row | build_rows(N, Tail)].
