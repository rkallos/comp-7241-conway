-module(conway_worker).

-include("conway.hrl").

-record(state, {cell_state  = undefined :: cell_state(),
                current_gen = 0         :: non_neg_integer(),
                neighbors   = []        :: [pid()],
                parent      = undefined :: pid(),
                target_gen  = 0         :: non_neg_integer()}).

-export([init/0]).

%% Public

init() ->
    receive
        {Parent, Options} -> start(Parent, Options)
    end.

%% Private

start(Parent, Options) ->
    #{target_gen := Gen,
      neighbors  := Neighbors,
      state      := CellState} = Options,
    State = #state{cell_state = CellState,
                   target_gen = Gen,
                   parent = Parent,
                   neighbors = Neighbors},
    worker_loop(State).

worker_loop(State = #state{parent = Parent,
                           current_gen = G,
                           target_gen = G}) ->
    Parent ! msg(State);
worker_loop(State) ->
    broadcast(State),
    #state{neighbors = NeighborPids,
           cell_state = CellState,
           current_gen = Gen} = State,
    NeighborStates =
        [receive
             {Pid, G, NeighborState} when G =:= Gen -> NeighborState
         end || Pid <- NeighborPids],
    LiveNeighbors = conway_common:count_neighbors(NeighborStates),
    NextCellState = conway_common:next_state(CellState, LiveNeighbors),
    NewState = State#state{cell_state = NextCellState, current_gen = Gen + 1},
    worker_loop(NewState).

broadcast(State = #state{neighbors = Neighbors}) ->
    Msg = msg(State),
    [Neighbor ! Msg || Neighbor <- Neighbors].

msg(#state{cell_state = CellState, current_gen = Gen}) ->
    {self(), Gen, CellState}.
