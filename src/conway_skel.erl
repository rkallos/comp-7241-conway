-module(conway_skel).
-include("conway.hrl").

-compile([export_all]).

-export([pipe/2,
         mr_row/1]).

%% Public
pipe(0, Board) -> Board;
pipe(G, Board) ->
    Workflow =
        [{pipe,
          [{seq, fun conway_seq:next_generation/1} || _ <- lists:seq(1,G)]}],
    hd(skel:do(Workflow, [Board])).

farm_row(_Board) ->
    ok.
farm_cell(_Board) ->
    ok.

mr_row(Board = #board{size = Size}) ->
    MapFun = row_map_fun(Board),
    MergeFun = merge_fun(Board),
    Workflow =
        [{seq, fun split_into_rows/1},
         {map, [{seq, MapFun}]},
         {reduce, fun lists:append/2, fun(X) -> X end},
         {seq, fun lists:sort/1},
         {seq, MergeFun}],
    hd(skel:do(Workflow, [Board])).

mr_cell(Board = #board{size = Size}) ->
    MapFun = cell_map_fun(Board),
    MergeFun = merge_fun(Board),
    Workflow =
        [{seq, fun split_into_cells/1},
         {map, [{seq, MapFun}]},
         {reduce, fun lists:append/2, fun(X) -> X end},
         {seq, fun lists:sort/1},
         {seq, MergeFun}],
    hd(skel:do(Workflow, [Board])).

%% Private
create_seq(Gen) ->
    Curry = fun(Input) ->
                    conway_seq:get_generation(Gen, Input)
            end,
    [{seq, Curry}].

split_into_rows(#board{size = N}) ->
    [[{Y, X} || X <- lists:seq(1,N)] || Y <- lists:seq(1,N)].

split_into_cells(#board{size = N}) ->
    [[{Y, X}] || X <- lists:seq(1,N),
                 Y <- lists:seq(1,N)].

row_map_fun(Board) ->
    Module = case is_tuple(Board#board.board) of
                 true  -> conway_common_tt;
                 false -> conway_common_ets
             end,
    fun(Row) ->
            [{Y, X} || {Y, X} <- Row, Module:do({Y, X}, Board) =:= on]
    end.

cell_map_fun(Board) ->
    Module = case is_tuple(Board#board.board) of
                 true  -> conway_common_tt;
                 false -> conway_common_ets
             end,
    fun([Cell]) ->
            case Module:do(Cell, Board) of
                off -> [];
                on -> [Cell]
            end
    end.

merge_fun(#board{size = Size, board = Board}) ->
    Module = case is_tuple(Board) of
                 true  -> conway_common_tt;
                 false -> conway_common_ets
             end,
    fun(LiveCells) ->
            Module:build_board(Size, LiveCells)
    end.
