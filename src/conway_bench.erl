-module(conway_bench).
-include("conway.hrl").

-export([bench/2]).

bench(Size, P) ->
    Cells = conway_common:random_board(Size, P),
    Impls = [seq, skel_row, skel_cell, erlangy],
    Boards = [{tt, conway_common_tt:build_board(Size, Cells)},
              {ets, conway_common_ets:build_board(Size, Cells)}],
    Keys = [{Impl, Board} || Impl <- Impls, Board <- Boards],
    lists:foldl(fun run_bench/2, maps:new(), Keys).

run_bench({K, {Impl, Board}}, Map) ->
    Fun = get_fun(K),
    Result = timing:function(fun() -> Fun(Board) end, 10, 1),
    maps:put({K, Impl}, Result, Map).

get_fun(seq) ->
    fun conway_seq:next_generation/1;
get_fun(skel_row) ->
    fun conway_skel:mr_row/1;
get_fun(skel_cell) ->
    fun conway_skel:mr_cell/1;
get_fun(erlangy) ->
    fun(Board) ->
            conway_erlangy:get_generation(1, Board)
    end.
