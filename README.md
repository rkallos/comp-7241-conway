COMP 7241: Term Project
=====

4 Implementations of Conway's Game of Life for COMP 7241: Parallel Algorithms and Architectures.

Richard Kallos and Mahdiye Nikraftar.

Build
-----

    $ rebar3 compile
