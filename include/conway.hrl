-type cell_state() :: on | off.
-type cell_pos() :: {non_neg_integer(), non_neg_integer()}.

-record(board, {size = 0 :: pos_integer(),
                board = undefined :: ets:tid() | {tuple()}}).
-type board() :: #board{}.
